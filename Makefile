# Makefile

clean:
	rm -rf gcm.cache *.o *.pcm

all: clean
	g++ -std=c++17 -fmodules-ts -x c++-system-header iostream
	g++ -std=c++17 -fmodules-ts -x c++-system-header string
	g++ -std=c++17 -fmodules-ts -c -x c++ cat.cpp -o cat.o
	g++ -std=c++17 -fmodules-ts -c -x c++ dog.cpp -o dog.o
	g++ -std=c++17 -fmodules-ts -c -x c++ animals.cpp -o animals.o
	g++ -std=c++17 -fmodules-ts main.cpp -o main.exe animals.o dog.o cat.o

all20: clean
	g++ -std=c++20 -fmodules-ts -x c++-system-header iostream
	g++ -std=c++20 -fmodules-ts -x c++-system-header string
	g++ -std=c++20 -fmodules-ts -c -x c++ cat.cpp -o cat.o
	g++ -std=c++20 -fmodules-ts -c -x c++ dog.cpp -o dog.o
	g++ -std=c++20 -fmodules-ts -c -x c++ animals.cpp -o animals.o
	g++ -std=c++20 -fmodules-ts main.cpp -o main.exe animals.o dog.o cat.o

clang20: clean
	clang++ -std=c++20 -x c++-system-header iostream --precompile
	clang++ -std=c++20 -x c++-system-header string   --precompile
	clang++ -std=c++20 -fmodule-file=string.pcm -x c++-module cat.cpp --precompile
	clang++ -std=c++20 -fmodule-file=string.pcm -x c++-module dog.cpp --precompile
	clang++ -std=c++20 -fprebuilt-module-path=. -fmodule-file=cat.pcm -fmodule-file=dog.pcm -x c++-module animals.cpp --precompile
	clang++ -std=c++20 main.cpp -fprebuilt-module-path=. -fmodule-file=string.pcm -fmodule-file=iostream.pcm animals.pcm cat.pcm dog.pcm -o main.exe
	# when do I need -fmodule-file and when not?
	@# -o xy.pcm is optional.
