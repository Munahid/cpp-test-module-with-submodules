C++ module with two submodules.

$ make
rm -rf gcm.cache *.o
g++ -std=c++17 -fmodules-ts -x c++-system-header iostream
g++ -std=c++17 -fmodules-ts -x c++-system-header string
g++ -std=c++17 -fmodules-ts -c -x c++ cat.cpp -o cat.o
g++ -std=c++17 -fmodules-ts -c -x c++ dog.cpp -o dog.o
g++ -std=c++17 -fmodules-ts -c -x c++ animals.cpp -o animals.o
g++ -std=c++17 -fmodules-ts main.cpp -o main.exe animals.o dog.o cat.o
C:/msys64/mingw64/bin/../lib/gcc/x86_64-w64-mingw32/11.2.0/../../../../x86_64-w64-mingw32/bin/ld.exe: cat.o:cat.cpp:(.text+0x0): multiple definition of `std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Alloc_hider::~_Alloc_hider()'; dog.o:dog.cpp:(.text+0x0): first defined here
C:/msys64/mingw64/bin/../lib/gcc/x86_64-w64-mingw32/11.2.0/../../../../x86_64-w64-mingw32/bin/ld.exe: cat.o:cat.cpp:(.text+0x0): multiple definition of `std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Alloc_hider::~_Alloc_hider()'; dog.o:dog.cpp:(.text+0x0): first defined here
collect2.exe: error: ld returned 1 exit status
make: *** [Makefile:10: all] Error 1


Works with -std=c++11.
Works with removing import string and returning an integer.

With -std=c++23:
$ make
rm -rf gcm.cache *.o
g++ -std=c++23 -fmodules-ts -x c++-system-header iostream
g++ -std=c++23 -fmodules-ts -x c++-system-header string
g++ -std=c++23 -fmodules-ts -c -x c++ cat.cpp -o cat.o
g++ -std=c++23 -fmodules-ts -c -x c++ dog.cpp -o dog.o
g++ -std=c++23 -fmodules-ts -c -x c++ animals.cpp -o animals.o
g++ -std=c++23 -fmodules-ts main.cpp -o main.exe animals.o dog.o cat.o
C:/msys64/mingw64/bin/../lib/gcc/x86_64-w64-mingw32/11.2.0/../../../../x86_64-w64-mingw32/bin/ld.exe: dog.o:dog.cpp:(.text+0x36): multiple definition of `std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Alloc_hider::~_Alloc_hider()'; C:\msys64\tmp\ccugePzi.o:main.cpp:(.text+0x0): first defined here
C:/msys64/mingw64/bin/../lib/gcc/x86_64-w64-mingw32/11.2.0/../../../../x86_64-w64-mingw32/bin/ld.exe: dog.o:dog.cpp:(.text+0x36): multiple definition of `std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Alloc_hider::~_Alloc_hider()'; C:\msys64\tmp\ccugePzi.o:main.cpp:(.text+0x0): first defined here
C:/msys64/mingw64/bin/../lib/gcc/x86_64-w64-mingw32/11.2.0/../../../../x86_64-w64-mingw32/bin/ld.exe: cat.o:cat.cpp:(.text+0x0): multiple definition of `std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Alloc_hider::_Alloc_hider(char*, std::allocator<char> const&)'; dog.o:dog.cpp:(.text+0x0): first defined here
C:/msys64/mingw64/bin/../lib/gcc/x86_64-w64-mingw32/11.2.0/../../../../x86_64-w64-mingw32/bin/ld.exe: cat.o:cat.cpp:(.text+0x0): multiple definition of `std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Alloc_hider::_Alloc_hider(char*, std::allocator<char> const&)'; dog.o:dog.cpp:(.text+0x0): first defined here
C:/msys64/mingw64/bin/../lib/gcc/x86_64-w64-mingw32/11.2.0/../../../../x86_64-w64-mingw32/bin/ld.exe: cat.o:cat.cpp:(.text+0x36): multiple definition of `std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Alloc_hider::~_Alloc_hider()'; C:\msys64\tmp\ccugePzi.o:main.cpp:(.text+0x0): first defined here
C:/msys64/mingw64/bin/../lib/gcc/x86_64-w64-mingw32/11.2.0/../../../../x86_64-w64-mingw32/bin/ld.exe: cat.o:cat.cpp:(.text+0x36): multiple definition of `std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Alloc_hider::~_Alloc_hider()'; C:\msys64\tmp\ccugePzi.o:main.cpp:(.text+0x0): first defined here
collect2.exe: error: ld returned 1 exit status
make: *** [Makefile:10: all] Error 1


